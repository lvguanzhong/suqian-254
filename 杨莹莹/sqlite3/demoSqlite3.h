#ifndef __DEMO_SQLITE3_H_
#define __DEMO_SQLITE3_H_

#include <sqlite3.h>
#include <string>

class SQLITE3
{
public:
    static SQLITE3* getInstance(const char* dbName)
    {
        if (!instance)
        {
            instance = new SQLITE3(dbName);
        }
        return instance; 
    }

    void createTable();

    void insertTable(const char* name, int age);

    void dropTable();

    void selectTable();

private:
    SQLITE3(const char* dbName)
    {
        mydb = nullptr;
        sqlite3_open(dbName, &mydb);
    }

    ~SQLITE3()
    {
        if (mydb)
        {
            sqlite3_close(mydb);
        }
    }

    static SQLITE3* instance;
    sqlite3 *mydb;
    std::string sql;


};





#endif