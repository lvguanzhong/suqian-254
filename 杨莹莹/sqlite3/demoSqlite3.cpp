#include <iostream>
#include <sqlite3.h>
#include <stdlib.h>
#include "demoSqlite3.h"


SQLITE3::SQLITE3(const char* dbName)
{
    /* 打开数据库 : 如果数据库不存在, 那么就创建 */
    int ret = sqlite3_open(dbName, &mydb);
    if (ret != SQLITE_OK)
    {
        perror("sqlite open error");
        exit(-1);
    }
}

void SQLITE3::createTable()
{
    char *errMsg = nullptr;
    sql = "create table if not exists user (name text not null, age int not null)";
    int ret = sqlite3_exec(mydb, sql.c_str(), NULL, NULL, &errMsg);
    if (ret != SQLITE_OK)
    {
        std::cerr<<"sqlite exec error:"<<std::endl;
        sqlite3_free(errMsg);
    }
    else
    {
        std::cout<<"create table success"<<std::endl;
    }
}

void SQLITE3::insertTable(const char* name, int age)
{
    char *errormsg = nullptr;
    sql = "insert into user(name, age) values ('" + std::string(name) + "','" + std::to_string(age) + "')";
    int ret = sqlite3_exec(mydb, sql.c_str(), NULL, NULL, &errormsg);
    if (ret != SQLITE_OK)
    {
        std::cerr<<"sqlite exec error:"<<std::endl;
        sqlite3_free(errormsg);
    }
    else 
    {
        std::cout<<"create table success"<<std::endl;

    }

}

void SQLITE3::dropTable()
{
    char * errormsg = nullptr;
    /* 删除表 : drop table + 表名 */
    sql = "drop table user";
    int ret = sqlite3_exec(mydb, sql.c_str(), NULL, NULL, &errormsg);
    if (ret != SQLITE_OK)
    {
        std::cerr<<"sqlite drop error:"<<std::endl;
        sqlite3_free(errormsg);
    }
    else
    {
        std::cout<<"drop table success"<<std::endl;

    }
}

void SQLITE3::selectTable()
{
    char * errormsg = nullptr;
    sql = "select * from user";
    int ret = sqlite3_exec(mydb, sql.c_str(), NULL, NULL, &errormsg);
    if (ret != SQLITE_OK)
    {
        std::cerr<<"sqlite select error:"<<std::endl;
        sqlite3_free(errormsg);
    }

}

SQLITE3::~SQLITE3()
{
    if(mydb)
    {
        /* 关闭数据库 */
        sqlite3_close(mydb);
    }
   
}

int mian()
{
    SQLITE3 *sqlite3 = SQLITE3::getInstance("test.db");

    sqlite3->createTable();
    sqlite3->insertTable("zhangsan", 18);
    sqlite3->selectTable();



    return 0;

}
