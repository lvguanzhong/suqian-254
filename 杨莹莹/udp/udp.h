#ifndef __UDP_CLIENT_H_
#define __UDP_CLIENT_H_

#include <arpa/inet.h>
#include <string>

class UDP
{
private:
    int sockfd;
    struct sockaddr_in serverAddress;
    struct sockaddr_in clientAddress;
    struct sockaddr_in localAddress;

    static UDP* instance;
    UDP(int port);
    UDP(int port, std::string serverId);
public:
    static UDP* getInstance(int port);

    static UDP* getInstance(int port, std::string serverId);
    /* 构造函数 同时初始化服务器 */
    UDP(int port);
    /* 构造函数 同时初始化客户端 */
    UDP(int port, std::string serverIp);
    void sendServer(const std::string &message);
    std::string receiveServer();
    void sendClient(const std::string& message);
    std::string receiveClient();

    ~UDP();
};


#endif