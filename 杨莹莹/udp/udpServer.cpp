#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <cstring>
#include <string>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <stdlib.h>
#include "udp.h"

#define SERVER_PORT 8080
#define BUFFER_SIZE 26

UDP* UDP::getInstance(int port)
{
    if (!instance)
    {
        instance = new UDP(port);
    }
    return instance;
}

UDP::UDP(int port)
{
    /* 创建新的套接字 */
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd == -1)
    {
        perror("socket error");
        exit(-1);
    }

    /* 清除脏数据 */
    memset(&localAddress, 0, sizeof(localAddress));

    /* 将本地的地址转换成服务器可以识别的状态 地址族 */
    localAddress.sin_family = AF_INET;
    localAddress.sin_port = htons(SERVER_PORT);
    localAddress.sin_addr.s_addr = INADDR_ANY;

    int localAddressLen = sizeof(localAddress);
    /* 绑定 */
    int ret = bind(sockfd, (struct sockaddr *)&localAddress, localAddressLen);
    if (ret == -1)
    {
        perror("bind error");
        exit(-1);
    }
}

void UDP::sendServer(const std::string &message)
{
    struct sockaddr_in clientAddress;
    memset(&clientAddress, 0, sizeof(clientAddress));

    sendto(sockfd, message.c_str(), message.length(), 0, (struct sockaddr *)&clientAddress, sizeof(clientAddress));
}

std::string UDP::receiveServer()
{
    /* 接收数据的数组 */
    char recvbuffer[BUFFER_SIZE];
    memset(recvbuffer, 0, sizeof(recvbuffer));

    socklen_t clientAddressLen = sizeof(clientAddress);
    recvfrom(sockfd, recvbuffer, sizeof(recvbuffer), 0, (struct sockaddr *)&clientAddress, &clientAddressLen);
    return std::string(recvbuffer);
}

UDP::~UDP()
{
    close(sockfd);
}
    
int main()
{
    UDP *server = UDP::getInstance(SERVER_PORT);

    server->sendServer("hello world");

    std::string  receivedMsg = server->receiveServer();
    std::cout<<"received message:"<<receivedMsg<<std::endl;

    return 0;
}


