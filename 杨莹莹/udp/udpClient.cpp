#include <iostream>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string>
#include <cstring>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <stdlib.h>
#include "udp.h"

#define SERVER_PORT 8080
#define BUFFER_SIZE 26
#define SERVER_IP "172.28.25.147"

UDP* UDP::getInstance(int port, std::string serverId)
{
    if (!instance)
    {
        instance = new UDP(SERVER_PORT, SERVER_IP);
    }
    return instance;
}


UDP::UDP(int port, std::string serverIp)
{
    /* 创建套接字 */
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd == -1)
    {
        perror("socket error");
        exit(-1);
    }

    memset(&serverAddress, 0, sizeof(serverAddress));
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(SERVER_PORT);

    int ret = inet_pton(AF_INET, SERVER_IP, (void *)&serverAddress.sin_addr.s_addr);
    if (ret == -1)
    {
        perror("inet_pton error");
        exit(-1);
    }
}

void UDP::sendClient(const std::string& message)
{
    char buffer[BUFFER_SIZE];
    memset(buffer, 0, sizeof(buffer));

    strncpy(buffer, message.c_str(), sizeof(buffer) - 1);
    sendto(sockfd, buffer, sizeof(buffer), 0, (struct sockaddr *)&serverAddress, sizeof(serverAddress));
}

std::string UDP::receiveClient()
{
    char recvBuffer[BUFFER_SIZE];
    memset(recvBuffer, 0, sizeof(recvBuffer) - 1);

    socklen_t sockAddressLen = sizeof(serverAddress);
    recvfrom(sockfd, recvBuffer, sizeof(recvBuffer) - 1, 0, (struct sockaddr*)&serverAddress, &sockAddressLen);
    return std::string(recvBuffer);
}

UDP::~UDP()
{
    close(sockfd);
}


int main()
{
    UDP *client = UDP::getInstance(SERVER_PORT, SERVER_IP);

    client->sendClient("hello world");

    std::string  receivedMsg = client->receiveClient();
    std::cout<<"received message:"<<receivedMsg<<std::endl;

    return 0;
}
