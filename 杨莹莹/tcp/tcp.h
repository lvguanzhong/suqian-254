#ifndef __TCP_H
#define __TCP_H

#include <string>
#include <arpa/inet.h>

class TCP
{
private:
    int sockfd;
    int acceptfd;
    struct sockaddr_in serverAddress;
    struct sockaddr_in clientAddress;
    struct sockaddr_in localAddress;

    static TCP* instance;
    TCP(int port);
    TCP(int port, std::string serverId);

public:
    static TCP* getInstance(int port);

    static TCP* getInstance(int port, std::string serverId);

    ~TCP();
    void writeServer(const std::string &message);
    std::string readServer();
    
    void writeClient(const std::string& message);
    std::string readClient();

};

TCP* TCP::instance = nullptr; // 初始化静态成员变量为nullptr

#endif
