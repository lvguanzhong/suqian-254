#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <cstring>
#include <string>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <error.h>
#include "tcp.h"

#define SERVER_PORT 8080
#define SERVER_IP   "172.25.23.103"
#define BUFFER_SIZE 128

TCP* TCP::getInstance(int port, std::string serverId)
{
    if (!instance)
    {
        instance = new TCP(SERVER_PORT, SERVER_IP);
    }
    return instance;
}

TCP::TCP(int port, std::string serverId)
{
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
    {
        perror("socket error");
        exit(-1);
    }
    memset(&serverAddress, 0, sizeof(serverAddress));
    /* 端口 */
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(SERVER_PORT);
    /* IP地址 */
    int ret = inet_pton(AF_INET, SERVER_IP, (void *)&(serverAddress.sin_addr.s_addr));
    if (ret != 1)
    {
        perror("inet_pton error");
        exit(-1);
    }

    /* ip地址 */
    ret = connect(sockfd, (struct sockaddr *)&serverAddress, sizeof(serverAddress));
    if (ret == -1)
    {
        perror("connect error");
        exit(-1);
    }

}

void TCP::writeClient(const std::string& message)
{
    char buffer[BUFFER_SIZE];
    memset(buffer, 0, sizeof(buffer));
    int writeBytes = write(sockfd, buffer, sizeof(buffer));
    if (writeBytes < 0)
    {
        perror("write error");
        exit(-1);
    }

}

std::string TCP::readClient()
{
    char readBuffer[BUFFER_SIZE];
    memset(readBuffer, 0, sizeof(readBuffer));
    read(sockfd, readBuffer, sizeof(readBuffer));
    std::cout<<"recv:"<<readBuffer<<std::endl;

    return std::string(readBuffer);

}

TCP::~TCP()
{
    close(sockfd);
}


int main()
{
    TCP *client = TCP::getInstance(SERVER_PORT, SERVER_IP);

    client->writeClient("12345");

    std::cout<<client->readClient()<<std::endl;


    return 0;
}