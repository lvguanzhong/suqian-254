#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string>
#include <cstring>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <error.h>
#include "tcp.h"

#define SERVER_PORT 8080
#define MAX_LISTEN  128
#define BUFFER_SIZE 128

TCP* TCP::getInstance(int port)
{
    if (!instance)
    {
        instance = new TCP(port);
    }
    return instance;
}

TCP::TCP(int port)
{
    /* 创建socket套接字 */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
    {
        perror("socket error");
        exit(-1);
    }

    /* 设置端口复用 */
    int enableOpt = 1;
    int ret = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (void *)&enableOpt, sizeof(enableOpt));
    if (ret == -1)
    {
        perror("setsockopt error");
        exit(-1);
    }
    /* 清除脏数据 */
    memset(&localAddress, 0, sizeof(localAddress));

    /* 地址族 */
    localAddress.sin_family = AF_INET;
    /* 端口需要转成大端 */
    localAddress.sin_port = htons(SERVER_PORT);
    /* ip地址需要转成大端 */
    localAddress.sin_addr.s_addr = htonl(INADDR_ANY); 

    int localAddressLen = sizeof(localAddress);
    ret = bind(sockfd, (struct sockaddr *)&localAddress, localAddressLen);
    if (ret == -1)
    {
        perror("bind error");
        exit(-1);
    }

    /* 监听 */
    ret = listen(sockfd, MAX_LISTEN);
    if (ret == -1)
    {
        perror("listen error");
        exit(-1);
    }

    /* 客户的信息 */
    memset(&clientAddress, 0, sizeof(clientAddress));

    socklen_t clientAddressLen = 0;
    acceptfd = accept(sockfd, (struct sockaddr *)&clientAddress, &clientAddressLen);
    if (acceptfd == -1)
    {
        perror("accpet error");
        exit(-1);
    }
}
void TCP::writeServer(const const std::string &message)
{
    char replyBuffer[BUFFER_SIZE];
    memset(replyBuffer, 0, sizeof(replyBuffer));
    int writeBytes = write(acceptfd, replyBuffer, sizeof(replyBuffer));
    if (writeBytes < 0)
    {
        perror("write error");
        exit(-1);
    }

}

std::string TCP::readServer()
{
    char buffer[BUFFER_SIZE];
    memset(buffer, 0, sizeof(buffer));
    int readBytes = read(acceptfd, (void *)&buffer, sizeof(buffer));
    if (readBytes <= 0)
    {
        perror("read eror");
        exit(-1);
    }
    return std::string(buffer);

}
int main()
{
    TCP *server = TCP::getInstance(SERVER_PORT);

    if (server->readServer() == "12345")
    {
        server->writeServer("一起加油254！");
    }

    return 0;
}