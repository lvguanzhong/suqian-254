#ifndef __REPLAY_THREAD_POOL_H_
#define __REPLAY_THREAD_POOL_H_




class ThreadPool
{
public:
    ThreadPool(int minThreads, int maxThreads, int queueCapacity);
    ~ThreadPool();

public:
    //void managerHander(); // 为什么不需要管理者线程？
    void threadHander();

    template<class Func, class... Args>
    void commit(Func&& func, Args&&... args)
    {
        if (!running)
        {
            std::cout<<"ThreadPool is stopped."<<std::endl;
            return;
        }
        

        using RetType = decltype(func(args...)); 
        auto task = std::make_shared<std::packaged_task<RetType()>>(
            std::bind(std::forward<Func>(func), std::forward<Args>(args)...)); // 绑定函数与参数
        std::future<RetType> future = task->get_future();
        {
            std::lock_guard<std::mutex> my_lock{ mutex_pool };
            queueTask.emplace([task](){(*task)();});
        }

        std::cout<<"已有任务："<< queueTask.size() <<std::endl;
        cond_task.notify_one(); // 唤醒一个线程执行
    }

private:
    ThreadPool(const ThreadPool&) = delete;  
    ThreadPool& operator=(const ThreadPool&) = delete; 

public:
    
    /* 设置 */
    int minThreads; // 设置最小线程数
    int maxThreads; // 设置最大线程数
    int queueCapacity;  // 任务队列容量
    
    using Task = std::function<void()>;  // 定义Task类型
    std::queue<Task> queueTask;              // 任务队列
    std::vector<std::thread> threadpool_tids; // 池中的所有线程

    std::mutex mutex_pool;              // 整个线程的同步锁
    std::condition_variable cond_task;  // 条件变量

    std::atomic<int> idlThrNum; // 空闲线程数 原子操作？
    std::atomic<bool> running; // 线程池运行标志
};



#endif 