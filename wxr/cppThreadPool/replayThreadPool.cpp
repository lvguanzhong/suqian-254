#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <queue>
#include <chrono>
#include <condition_variable>
#include <atomic>
#include <future>
#include <functional>
#include <stdexcept>

#include "replayThreadPool.h"


ThreadPool::ThreadPool(int minThreads, int maxThreads, int queueCapacity)
                    : minThreads(minThreads)
                    , maxThreads(maxThreads)
                    , queueCapacity(queueCapacity)
{
    
    running = true;
    idlThrNum = queueCapacity;
    

    /* 判断合法性 */
    if (minThreads <= 0 || maxThreads <= 0 || minThreads >= maxThreads)
    {
        minThreads = 3;
        maxThreads = 5;
    }

    if (queueCapacity <= 0)
    {
        queueCapacity = 16;
    }
    
    std::cout<<""<<std::endl;
    // 添加线程
    threadpool_tids.resize(queueCapacity);
    for (std::thread& tid : threadpool_tids)
    {
        tid = std::move(std::thread(&ThreadPool::threadHander, this));
    }
}

ThreadPool::~ThreadPool()
{
    running = false;
    cond_task.notify_all(); 
    for (std::thread& thread : threadpool_tids)
    {
        if(thread.joinable())
            thread.join();
    }
}

void ThreadPool::threadHander()
{
    std::cout<<std::this_thread::get_id()<< " 申请成功"<<std::endl;
    int count = 0;

    while(running) // 线程池还在跑
    {
        Task task; // 准备获取任务
        {
            std::cout<<std::this_thread::get_id()<< " 正在等待"<<std::endl;
            std::unique_lock<std::mutex> tmp_lock{ mutex_pool };

            // 只有当pred为false时，wait才会阻塞当前线程
            // 只有当running为true, threadpool_tids.empty()为true 时，wait才会阻塞当前线程
            cond_task.wait(tmp_lock, [this] {return !running || !queueTask.empty();}); 

            if (!running && queueTask.empty()) // 线程销毁的功能
                return;
            
            std::cout<<std::this_thread::get_id()<< " 累计执行 "<< count++ <<std::endl;
            task = move(queueTask.front()); // 获取任务
			queueTask.pop();
        }
        idlThrNum--;
        task();
        idlThrNum++;
    }
}

