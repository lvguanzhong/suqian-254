#include <iostream>
#include <atomic>
#include <condition_variable>
#include <functional>
#include <future>
#include <mutex>
#include <queue>
#include <string>
#include <thread>
#include <vector>
#include "replayThreadPool.h"
#include <stdlib.h>
#include <unistd.h>

void fun1(int slp)
{
	if (slp > 0) {
		std::cout<<"sleep func "<< slp << " tid: " <<std::this_thread::get_id()<<std::endl;
		//std::this_thread::sleep_for(std::chrono::milliseconds(slp));
		sleep(rand()%3);
	}
}

 
int main()
{
	srand(time(NULL));
    ThreadPool pool(2,3,11);

    
    for (int i = 1; i < 100; i++)
    {
		pool.commit(fun1, i);
    }

	sleep(10);
	std::cout<<"已有任务："<< pool.queueTask.size() <<std::endl;
	return 0;
}