#include <iostream>

#include "stdNet.h"

int main()
{
    int ret = 0;
    int clientfd = 0;
    Server s1(8080, SOCK_STREAM, 128, 10);
    s1.Slisten();
    clientfd = s1.Saccept();
    while (1)
    {
        ret = s1.Sread(clientfd);
        if (ret < 0)
        {
            break;
        }
        
        ret = s1.Swrite(clientfd, ret);
        if (ret < 0)
        {
            break;
        }
        sleep(1);
    }
    

    return 0;
}