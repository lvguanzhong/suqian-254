#ifndef __STD_SQLITE3_
#define __STD_SQLITE3_
#include <sqlite3.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdarg.h>


class SQLite3
{
public:

public:
    SQLite3();
    ~SQLite3();
    //bool open_sqlite(const char *path);
    bool input_sql(const char *format, ...);           // 输入语句
    bool judge(char *errormsg, const char *format, ...);           // 判断语句结果 ~ 判断表中是否有符合条件的至少一条数据
    char**& get_table(int *pRow, int *pCol, char *errormsg, const char *format, ...);     // 获取查询返回的表单
    bool table_free();       // 释放表单
private:
    sqlite3 *m_db;
    char *sql_cmd;
    char **m_table;
};








#endif // __STD_SQLITE3_