#ifndef __STD_NET_H_
#define __STD_NET_H_

#include <cstdio>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include <netinet/in.h>
#include <arpa/inet.h>



class Server
{
public:


public:
    bool Slisten();   // 监听 √
    int Saccept();    // 接受连接 √
    int Sread(int client_fd);      // 读消息  客户端下线/指定客户端套接字
    int Swrite(int client_fd, int send_size);    // 发送消息
    int Srecv();
    int Ssend();


    int m_readBytes;
public:
    Server(); // 构造+初始化变量+初始化堆空间+创建套接字+绑定本地+设置端口复用
    Server(int server_port, int TorU, int buffer_size, int max_listen);
    ~Server(); // 析构

private:
    static int num_fd;
    int m_buffer_size;
    int m_sockfd;
    struct sockaddr_in localAddress; // 服务端==自己
    
    int m_solo_clientfd;
    
    struct sockaddr_in clientAddress; // 客户端
    socklen_t clientAddressLen = sizeof(clientAddress);

    char *recv_buffer;
    char *send_buffer;
    int m_port;
    int m_max_listen;

    void init(int TorU);
};


class Client
{
public:
    Client();
    Client(const char * server_ip, int server_port, int TorU, int buffer_size);
    ~Client();
public:
    int Cconnect();
    int Cread();
    int Cwrite();
    int CwriteInput();
    int Crecv();
    int Csend();
    int readBytes;

private:
    static int num_fd;
    int m_buffer_size;
    int m_sockfd;
    int m_server_port;
    
    struct sockaddr_in m_server_addr;// 服务端
    socklen_t server_addr_len = 0;  // 服务端信息长度
    struct sockaddr_in m_local_addr; // 客户端==自己

    char *recv_buffer;
    char *send_buffer;

    void init(int TorU, const char *server_ip);
};





#endif