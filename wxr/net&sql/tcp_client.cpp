#include <iostream>

#include "stdNet.h"

int main()
{
    Client c1("192.168.42.128", 8080, SOCK_STREAM, 128);
    c1.Cconnect();

    int ret = 0;
    while (1)
    {
        ret = c1.CwriteInput();
        if (ret < 0)
        {
            break;
        }
        ret = c1.Cread();
        if (ret < 0)
        {
            break;
        }
    }
    return 0;
}