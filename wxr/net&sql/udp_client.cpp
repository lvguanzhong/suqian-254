#include <iostream>

#include "stdNet.h"

int main()
{
    Client c1("192.168.42.128", 8080, SOCK_DGRAM, 128);
    //c1.Cconnect();

    int ret = 0;
    while (1)
    {
        ret = c1.Csend();
        if (ret < 0)
        {
            break;
        }
        ret = c1.Crecv();
        if (ret < 0)
        {
            break;
        }
        sleep(1);
    }
    return 0;
}