#include <iostream>

#include "stdSqlite.h"

#define DEFAULT_PATH "./default_base.db"

int main()
{

    SQLite3 db;
    SQLite3 db2;
    std::cout<< "创建数据表" <<std::endl;
    db.input_sql("CREATE TABLE if not exists User(\
                    userName CHAR(12)   NOT NULL,\
                    userPwd CHAR(50)    NOT NULL,\
                    lastLogoutTime INT DEFAULT 0 NOT NULL,\
                    PRIMARY KEY(userName));");
    std::cout<< "插入数据" <<std::endl;
    std::cout<< db.input_sql("INSERT INTO User VALUES('wang', '123', '0');") <<std::endl;
    std::cout<< db.input_sql("INSERT INTO User VALUES('xiao', '123', '0');") <<std::endl;
    
    const char *name = "wang";
    std::cout<< "查询数据" <<std::endl;
    std::cout<< db.input_sql("SELECT * FROM User WHERE userName = '%s';", name) <<std::endl;

    char *errormsg;
    int row = 0;
    int col = 0;
    char **result = db.get_table(&row, &col, errormsg, "SELECT * FROM User WHERE userPwd = '123';");

    std::cout<< "获取数据表" <<std::endl;
    std::cout<< "row:" << row <<std::endl;
    std::cout<< "col:" << col <<std::endl;

    for (int idx = 0; idx <= row; idx++)
    {
        for(int jdx = 0; jdx < col; jdx++)
            printf("%s\t", result[idx * col +jdx]);

        printf("\n");
    }
        



    return 0;
}