#include <iostream>

#include "stdNet.h"

int Server::num_fd = 0;

int Client::num_fd = 0;

/*==================服务端====================*/
Server::Server()
{
    std::cout<<"构造"<<std::endl;
    // 初始化
    memset(&clientAddress, 0, sizeof(clientAddress));
    memset(&localAddress, 0, sizeof(localAddress));
    m_port = 8080;
    m_max_listen = 10;
    m_buffer_size = 128;

    recv_buffer = new char[m_buffer_size];
    send_buffer = new char[m_buffer_size];


    // 初始化
    init(SOCK_STREAM);

    num_fd++;
}

Server::Server(int server_port, int TorU, int buffer_size, int max_listen)
{
    std::cout<<"自定义构造"<<std::endl;
    // 初始化
    memset(&clientAddress, 0, sizeof(clientAddress));
    memset(&localAddress, 0, sizeof(localAddress));
    m_port = server_port;
    m_max_listen = max_listen;
    m_buffer_size = buffer_size;

    recv_buffer = new char[m_buffer_size];
    send_buffer = new char[m_buffer_size];


    // 初始化
    init(TorU);

    num_fd++;
}

void Server::init(int TorU)
{
    // 生成套接字
    m_sockfd = socket(AF_INET, TorU, 0);
    if (m_sockfd == -1)
    {
        perror("socket error");
        exit(-1);
    }
    // 绑定服务器地址
    localAddress.sin_family = AF_INET;                  /* 地址族 */
    localAddress.sin_port = htons(m_port);         /* 端口需要转成大端 */
    localAddress.sin_addr.s_addr = htonl(INADDR_ANY);   /* ip地址需要转成大端 */
    if (bind(m_sockfd, (struct sockaddr *)&localAddress, sizeof(localAddress)) == -1)
    {
        close(m_sockfd);
        perror("bind error");
        exit(-1);
    }

    // 设置端口复用
    int enableOpt = 1;
    if (setsockopt(m_sockfd, SOL_SOCKET, SO_REUSEADDR, (void *)&enableOpt, sizeof(enableOpt)) == -1)
    {
        close(m_sockfd);
        perror("setsockopt error");
        exit(-1);
    }
}

Server::~Server()
{
    std::cout<<"析构"<<std::endl;
    close(m_sockfd);

    delete[] send_buffer;
    delete[] recv_buffer;

    num_fd--;
}

bool Server::Slisten()
{
    int ret = listen(m_sockfd, m_max_listen);
    if (ret == -1)
    {
        perror("listen error");
        return false;
    }
    std::cout<<"监听成功"<<std::endl;
    return true;
}

int Server::Saccept()
{
    m_solo_clientfd = accept(m_sockfd, (struct sockaddr *)&clientAddress, &clientAddressLen);
    if (m_solo_clientfd == -1)
    {
        perror("accpet error");
        exit(-1);
    }
    std::cout<<"连接到客户端"<<std::endl;
    return m_solo_clientfd;
}

int Server::Sread(int client_fd)
{
    if(client_fd <= 0)
    {
        std::cout<<"客户端套接字错误"<<std::endl;
        return -1;
    }
    memset(recv_buffer, 0, sizeof(char) * m_buffer_size);
    m_readBytes = read(client_fd, recv_buffer, m_buffer_size);
   
    if (m_readBytes == 0)
    {
        std::cout<<"客户端断开连接"<<std::endl;
        close(client_fd);
        return -1;
    }
    else if(m_readBytes < 0)
    {
        perror("read error");
        close(client_fd);
        return -1;
    }
    else if(m_readBytes > 0)
    {
        std::cout<<"recv:["<<m_readBytes<<"]["<<recv_buffer<<"]"<<std::endl;
        // 接收函数的处理
        memset(send_buffer, 0, sizeof(char) * m_buffer_size);
        strncpy(send_buffer, recv_buffer, strlen(recv_buffer));
    }
    return m_readBytes;
}

int Server::Swrite(int client_fd, int send_size)
{
    if (client_fd <= 0)
    {
        std::cout<<"客户端套接字错误"<<std::endl;
        return -1;
    }
    // 将缓冲区的输出
    int ret = write(client_fd, send_buffer, send_size);
    if (ret > 0)
    {
        printf("send:[%d][%s]\n", ret, send_buffer);
        return ret;
    }
    else if(ret == -1)
    {
        perror("write error");
        return -1;
    }
    return 0;
}
    
int Server::Srecv()
{
    memset(recv_buffer, 0, sizeof(char) * m_buffer_size);
    //memset(&clientAddress, 0, sizeof(clientAddress));
    int readBytes = recvfrom(m_sockfd, recv_buffer,sizeof(char) * m_buffer_size, 0, (sockaddr*)&clientAddress, &clientAddressLen);
    if (readBytes > 0)
    {
        printf("recvfrom:[%d][%s]\n", readBytes, recv_buffer);
        return readBytes;
    }
    else if(readBytes == 0)
    {
        printf("客户端下线\n");
        
        return -1;
    }
    else
    {
        perror("recvfrom error");
        return -1;
    }
}

int Server::Ssend()
{
    memset(send_buffer, 0, sizeof(char) * m_buffer_size);
    strncpy(send_buffer, recv_buffer, strlen(recv_buffer));

    // 将数据发回
    std::cout<<strlen(recv_buffer)<<std::endl;
    
    int readBytes = sendto(m_sockfd, send_buffer, strlen(send_buffer), 0, (sockaddr*)&clientAddress, clientAddressLen);
    if(readBytes == -1)
    {
        perror("sendto error");
    }
    else if(readBytes > 0)
    {
        printf("sendto:[%d][%s]\n", readBytes, send_buffer);
    }

    return readBytes;
    
}


/*==================客户端====================*/
Client::Client()
{
    std::cout<<"构造"<<std::endl;
    // 变量初始化
    memset(&m_server_addr, 0, sizeof(m_server_addr)); // 服务端信息
    m_server_port = 8080; // 端口
    m_buffer_size = 128; // 缓冲区大小

    // 堆区初始化
    recv_buffer = new char[m_buffer_size];
    send_buffer = new char[m_buffer_size];
    memset(send_buffer, 0, sizeof(char) * m_buffer_size);
    memset(recv_buffer, 0, sizeof(char) * m_buffer_size);

    // 初始化
    init(SOCK_STREAM, "192.168.42.128");

    num_fd++;
}

Client::Client(const char * server_ip, int server_port, int TorU, int buffer_size)
{
    std::cout<<"自定义构造"<<std::endl;
    // 变量初始化
    memset(&m_server_addr, 0, sizeof(m_server_addr)); // 服务端信息
    m_server_port = server_port; // 端口
    m_buffer_size = buffer_size; // 缓冲区大小

    // 堆区初始化
    recv_buffer = new char[m_buffer_size];
    send_buffer = new char[m_buffer_size];
    memset(send_buffer, 0, sizeof(char) * m_buffer_size);
    memset(recv_buffer, 0, sizeof(char) * m_buffer_size);

    // tcp初始化
    init(TorU, server_ip);
    num_fd++;
}

void Client::init(int TorU, const char *server_ip)
{
    // 创建套接字
    m_sockfd = socket(AF_INET, TorU, 0); //SOCK_DGRAM
    if (m_sockfd == -1)
    {
        perror("socket perror");
        exit(-1);
    }

    /* 端口 */
    m_server_addr.sin_family = AF_INET;
    m_server_addr.sin_port = htons(m_server_port);
    /* IP地址 */
    int ret = inet_pton(AF_INET, server_ip, (void *)&(m_server_addr.sin_addr.s_addr));
    if (ret != 1)
    {
        perror("inet_pton perror");
        exit(-1);
    }

    /* 设置端口复用 */
    int enableOpt = 1;
    ret = setsockopt(m_sockfd, SOL_SOCKET, SO_REUSEADDR, (void *)&enableOpt, sizeof(enableOpt));
    if (ret == -1)
    {
        perror("setsockopt error");
        exit(-1);
    }
}

Client::~Client()
{
    std::cout<<"析构"<<std::endl;

    delete[] send_buffer;
    delete[] recv_buffer;

    num_fd--;
}

int Client::Cconnect()
{
    int ret = connect(m_sockfd, (struct sockaddr *)&m_server_addr, sizeof(m_server_addr));
    if (ret == -1)
    {
        perror("connect perror");
        exit(-1);
    }

    return true;
}

int Client::Cread()
{
    memset(recv_buffer, 0, sizeof(char) * m_buffer_size);
    readBytes = read(m_sockfd, recv_buffer, m_buffer_size);
    if(readBytes < 0)
    {
        printf("服务端下线\n");
        close(m_sockfd);
        return -1;
    }
    else if (readBytes == 0)
    {
        perror("read eror");
        printf("服务端下线\n");// todo
        close(m_sockfd);
        return -1;
    }
    else if (readBytes > 0)
    {
        printf("recv:[%d][%s]\n", readBytes, recv_buffer);
        return readBytes;    
    }
    return readBytes;
}

int Client::Cwrite()
{
    // 将发送数据放入缓冲区
    memset(send_buffer, 0, sizeof(char) * m_buffer_size);
    strncpy(send_buffer, "helloworld", strlen("helloworld"));

    int ret = write(m_sockfd, send_buffer, strlen(send_buffer));
    if (ret == -1)
    {
        perror("write error");
        close(m_sockfd);
        return -1;
    }
    else if(ret > 0)
    {
        printf("send:[%d][%s]\n", ret, send_buffer);
        return ret;
    }
    return ret;
}

int Client::CwriteInput()
{
    // 将发送数据放入缓冲区
    memset(send_buffer, 0, sizeof(char) * m_buffer_size);
    printf("input:");
    scanf("%s", send_buffer);

    int ret = write(m_sockfd, send_buffer, strlen(send_buffer));
    if (ret == -1)
    {
        perror("write error");
        close(m_sockfd);
        return -1;
    }
    else if(ret > 0)
    {
        printf("send:[%d][%s]\n", ret, send_buffer);
        return ret;
    }
    return ret;
}

int Client::Crecv()
{
    int readBytes = recvfrom(m_sockfd, recv_buffer, m_buffer_size, 0, (sockaddr*)&m_server_addr, &server_addr_len);
    if (readBytes > 0)
    {
        printf("recvfrom:[%d][%s]\n", readBytes, recv_buffer);
        return readBytes;
    }
    else if(readBytes == 0)
    {
        printf("客户端下线\n");
        return -1;
    }
    else
    {
        perror("recvfrom error");
        return -1;
    }
}

int Client::Csend()
{
    //
    memset(send_buffer, 0, sizeof(char) * m_buffer_size);
    strncpy(send_buffer, "helloworld", strlen("helloworld"));

    int readBytes = sendto(m_sockfd, send_buffer, strlen(send_buffer), 0, (struct sockaddr*)&m_server_addr, sizeof(m_server_addr));
    if(readBytes == -1)
    {
        perror("sendto error");
    }
    else if(readBytes > 0)
    {
        printf("sendto:[%d][%s]\n", readBytes, send_buffer);
    }

    return readBytes;

    
}

//sendto(sockfd, ptr, strlen(ptr), 0, (struct sockaddr*)&sockAddress, sizeof(sockAddress));

    
//    struct sockaddr serverAddress;
//    memset(&serverAddress, 0, sizeof(serverAddress));

//    socklen_t sockAddressLen = sizeof(serverAddress);
//    recvfrom(sockfd, recvBuffer, sizeof(recvBuffer), 0, (struct sockaddr*)&serverAddress, &sockAddressLen);
//    printf("recvBuffer: %s\n", recvBuffer);