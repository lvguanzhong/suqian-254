#include <iostream>

#include "stdSqlite.h"

const int sql_cmd_size = 512;


SQLite3::SQLite3()
{
    std::cout<<"构造"<<std::endl;
    sql_cmd = new char[sql_cmd_size];
    m_table = NULL;
    if (sqlite3_open("default_database.db", &m_db) != SQLITE_OK)
    {
        perror("sqlite open error");
        exit(-1);
    }
}

SQLite3::~SQLite3()
{
    std::cout<<"析构"<<std::endl;
    sqlite3_close(m_db);
    if (m_table != NULL)
    {
        sqlite3_free_table(m_table);
    }
    
    delete[] sql_cmd;
}


bool SQLite3::input_sql(const char *format, ...)
{
    memset(sql_cmd, 0, sizeof(char) * sql_cmd_size);
    
    va_list args;
    va_start(args, format);
    vsprintf(sql_cmd, format, args); 
    va_end(args);

    char *errormsg = NULL;
    int ret = sqlite3_exec(m_db, sql_cmd, NULL, NULL, &errormsg);
    if (ret != SQLITE_OK)
    {
        printf("%s\t", sql_cmd);    
        printf("sqlite exec error:%s\n", errormsg);
        printf("ret=%d\n", ret);
        sqlite3_free(errormsg);
    }
    if (ret == 0)
    {
        return true;
    }
    else if (ret > 0)
    {
        return false;
    }
    printf("意外\n");
    
    return false;

}


bool SQLite3::judge(char *errormsg, const char *format, ...)
{
    
    memset(sql_cmd, 0, sizeof(char) * sql_cmd_size);
    sqlite3_free_table(m_table);
    va_list args;
    va_start(args, format);
    vsprintf(sql_cmd, format, args); // todo 检查溢出，处理报错
    va_end(args);

    // 返回值 1 有 0 没有 -1 读取出错
    m_table = NULL;
    int row = 0;
    int ret = sqlite3_get_table(m_db, sql_cmd, &m_table, &row, NULL, &errormsg);
    if (ret != SQLITE_OK)
    {
        printf("sqlite3_get_table error:%s\n", errormsg);
        printf("judgeExist ret=%d\n", ret);
        return -1;
    }
    if (row == 0)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}
char**& SQLite3::get_table(int *pRow, int *pCol, char *errormsg, const char *format, ...)
{
    
    memset(sql_cmd, 0, sizeof(char) * sql_cmd_size);
    sqlite3_free_table(m_table);
    m_table = NULL;

    va_list args;
    va_start(args, format);
    vsprintf(sql_cmd, format, args); // todo 检查溢出，处理报错
    va_end(args);

    
    int ret = sqlite3_get_table(m_db, sql_cmd, &m_table, pRow, pCol, &errormsg);
    if (ret != SQLITE_OK)
    {
        printf("sqlite3_get_table error:%s\n", errormsg);
        printf("ret=%d\n", ret);
    }

    return m_table;
}
