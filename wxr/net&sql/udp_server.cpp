#include <iostream>

#include "stdNet.h"

int main()
{
    int ret = 0;

    Server s1(8080, SOCK_DGRAM, 128, 10);

    while (1)
    {
        ret = s1.Srecv();
        if (ret < 0)
        {
            break;
        }
        ret = s1.Ssend();
        if (ret< 0)
        {
            break;
        }
        
    }
    return 0;
}