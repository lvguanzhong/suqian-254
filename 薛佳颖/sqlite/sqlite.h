#ifndef __SQLITE_H_
#define __SQLITE_H_

#include <sqlite3.h>
#include <iostream>

/* 单例是我只拿到一个数据库，在这个数据库里我实现打开关闭以及其他操作 */
class SQLite3
{
public:
    /* 查询结果 */
    char ** m_result;
    int m_row;
    int m_column;

    /* 单例 */
    static SQLite3* Instance(std :: string dbName, sqlite3* db);

    /* 打开数据库 */
    bool open(std :: string database, sqlite3* db);

    /* 创建表 */
    bool createTable(const std::string &sql);

    /* 删除表 */
    bool deleteTable(const std::string &tableName);

    /* 查询表中数据 */
    bool selectTableValue(const std::string &sql);

    /* 删除表中数据 */
    bool deleteTableValue(const std::string &sql);

    /* 增加表中数据 */
    bool insertTableValue(const std::string &sql);

    /* 修改表中数据 */
    bool modifyTableValue(const std::string &sql);

    /* 析构 */
    ~SQLite3();

private:
    sqlite3 *m_db;
    std :: string m_dbName;
    char *m_errMsg;
    SQLite3(std :: string dbName, sqlite3* db);
};


#endif //__SQLITE_H_