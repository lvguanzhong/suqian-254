#include "sqlite.h"
#include <iostream>

/* 开始 */
SQLite3 *SQLite3::Instance(std :: string dbName, sqlite3* db)
{
    static SQLite3 sqlite(dbName,  db);
    return &sqlite;
}

SQLite3 :: SQLite3(std :: string dbName, sqlite3* db):m_db(db), m_dbName(dbName)
{
}

/* 打开数据库 */
bool SQLite3::open(std :: string dbName, sqlite3* db)
{
    int ret = sqlite3_open(m_dbName.c_str(), &m_db);
    if (ret != SQLITE_OK)
    {
        perror("open db error");
        return false;
    }
    return true;
}

/* 创建表格 */
bool SQLite3::createTable(const std::string& sql)
{
    m_errMsg = nullptr;
    
    int ret = sqlite3_exec(m_db, sql.c_str(), NULL, NULL, &m_errMsg);
    if (ret != SQLITE_OK)
    {
        std::cout << "create table error: " << m_errMsg << std::endl;
        return false;
    }

    return true;
}

/* 删除表 */
bool SQLite3::deleteTable(const std::string &tableName)
{
    m_errMsg = nullptr;
    std::string sql = "drop table ";
    sql.append(tableName);
    int ret = sqlite3_exec(m_db, sql.c_str(), NULL, NULL, &m_errMsg);
    if (ret != SQLITE_OK)
    {
        std::cout << "drop table error: " << m_errMsg << std::endl;
        return false;
    }

    return true;
}

/* 查询表中数据 */
bool SQLite3::selectTableValue(const std::string &sql)
{
    m_errMsg = nullptr;

    int ret = sqlite3_get_table(m_db, sql.c_str(), &m_result, &m_row, &m_column, &m_errMsg);
    if (ret != SQLITE_OK)
    {
        std::cout << "select error: " << m_errMsg << std::endl;
        return false;
    }

    return true;
}

/* 删除表中数据 */
bool SQLite3::deleteTableValue(const std::string &sql)
{
    m_errMsg = nullptr;

    int ret = sqlite3_exec(m_db, sql.c_str(), NULL, NULL, &m_errMsg);
    if (ret != SQLITE_OK)
    {
        std::cout << "drop data error: " << m_errMsg << std::endl;
        return false;
    }

    return true;
}

/* 插入数据 */
bool SQLite3::insertTableValue(const std::string &sql)
{
    m_errMsg = nullptr;

    int ret = sqlite3_exec(m_db, sql.c_str(), NULL, NULL, &m_errMsg);
    if (ret != SQLITE_OK)
    {
        std::cout << "inster data error: " << m_errMsg << std::endl;
        return false;
    }

    return true;
}

/* 修改表中数据 */
bool SQLite3::modifyTableValue(const std::string &sql)
{
    m_errMsg = nullptr;

    int ret = sqlite3_exec(m_db, sql.c_str(), NULL, NULL, &m_errMsg);
    if (ret != SQLITE_OK)
    {
        std::cout << "modify data error: " << m_errMsg << std::endl;
        return false;
    }

    return true;
}

/* 析构 */
SQLite3::~SQLite3()
{
    sqlite3_close(m_db);
}